import Modele.Pacient;
import Observer.ClinicaNotificari;

import java.util.Scanner;

public class Main {
    public static void main (String [] arg)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduceti numele: ");
        String nume = scanner.nextLine();

        System.out.print("Introduceti prenumele: ");
        String prenume = scanner.nextLine();

        System.out.print("Introduceti numarul de telefon: ");
        String numarTelefon = scanner.nextLine();

        System.out.print("Introduceti anul nasterii: ");
        int anNasterii = Integer.parseInt(scanner.nextLine());

        System.out.print("Introduceti comentariu(optional): ");
        String comentariu = scanner.nextLine();

        System.out.print("Introduceti tipul consulatiei: ");
        String tipulConsultatiei= scanner.nextLine();

        ClinicaNotificari clinicaNotificari = new ClinicaNotificari();
        Pacient pacient = new Pacient(clinicaNotificari);
        pacient.nume = nume;
        pacient.prenume = prenume;
        pacient.numarTelefon = numarTelefon;
        pacient.anNasterii = anNasterii;
        pacient.comentariu = comentariu;
        pacient.tipulConsulatiei = tipulConsultatiei;

        InregistrareFacade inregistrareFacade = new InregistrareFacade();
        var raspuns = inregistrareFacade.Inregistrare(pacient);
        if (raspuns != null) {
            System.out.println("\nAi fost inregistrat la data de: " + raspuns);
        } else {
            System.out.println("Datele nu au fost validate");
        }

        clinicaNotificari.Attach(pacient);
        clinicaNotificari.statut = "deschis";
        clinicaNotificari.Notify();
    }
}
