package Servicii;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServiciuMedic {
    public String SelectareMedicDisponibil()
    {
        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDate = myDateObj.format(myFormatObj);
        System.out.println("Se selecteaza medicul disponibil");
        return formattedDate;
    }
}

