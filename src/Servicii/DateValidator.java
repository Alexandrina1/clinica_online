package Servicii;

import ChainOfResponsibility.AnNastereHandler;
import ChainOfResponsibility.ConsulatieHandler;
import ChainOfResponsibility.NumePrenumeHandler;
import ChainOfResponsibility.TelefonHandler;
import Modele.Pacient;

public class DateValidator {
    public boolean Validare(Pacient pacient)
    {
        NumePrenumeHandler numePrenumeHandler = new NumePrenumeHandler();
        TelefonHandler telefonHandler = new TelefonHandler();
        AnNastereHandler anNastereHandler = new AnNastereHandler();
        ConsulatieHandler consulatieHandler = new ConsulatieHandler();

        numePrenumeHandler.SetSuccessor(telefonHandler);
        telefonHandler.SetSuccessor(anNastereHandler);
        anNastereHandler.SetSuccessor(consulatieHandler);

        return numePrenumeHandler.HandleRequest(pacient);
    }
}
