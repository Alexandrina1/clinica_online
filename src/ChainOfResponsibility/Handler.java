package ChainOfResponsibility;

import Modele.Pacient;

public abstract class Handler {

    protected Handler successor;
    public void SetSuccessor(Handler successor)
    {
        this.successor = successor;
    }
    public abstract boolean HandleRequest(Pacient pacient);
}
