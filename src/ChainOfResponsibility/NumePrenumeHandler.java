package ChainOfResponsibility;

import Modele.Pacient;

public class NumePrenumeHandler extends Handler {

    @Override
    public boolean HandleRequest(Pacient pacient) {
        if (pacient.nume.length() > 4 && pacient.prenume.length() > 4) {
            if (successor != null) {
               return successor.HandleRequest(pacient);
            }
        }
        return false;
    }
}
