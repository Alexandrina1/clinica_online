package ChainOfResponsibility;

import Modele.Pacient;

public class ConsulatieHandler extends Handler {
    @Override
    public boolean HandleRequest(Pacient pacient) {
        return pacient.tipulConsulatiei.equals("gratis");
    }
}
