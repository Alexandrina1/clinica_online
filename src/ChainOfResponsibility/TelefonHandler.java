package ChainOfResponsibility;

import Modele.Pacient;

public class TelefonHandler extends Handler {
    @Override
    public boolean HandleRequest(Pacient pacient) {
        if (pacient.numarTelefon.matches("[0-9]+") && pacient.numarTelefon.length() > 2) {
            if (successor != null) {
                return successor.HandleRequest(pacient);
            }
        }
        return false;
    }
}
