package ChainOfResponsibility;

import Modele.Pacient;

public class AnNastereHandler extends Handler {
    @Override
    public boolean HandleRequest(Pacient pacient) {
        if (pacient.anNasterii > 1980 && pacient.anNasterii < 2015) {
            if (successor != null) {
                return successor.HandleRequest(pacient);
            }
        }
        return false;
    }
}
