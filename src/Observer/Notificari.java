package Observer;
import Modele.Pacient;
import java.util.*;

public class Notificari {
    List<Persoana> pacienti = new ArrayList<Persoana>();
    public void Attach(Persoana pacient)
    {
        pacienti.add(pacient);
    }
    public void Detach(Persoana pacient)
    {
        pacienti.remove(pacient);
    }
    public void Notify() {
        for (Persoana p :pacienti) {
            p.update();
        }
    }
}
