package Modele;

import Observer.ClinicaNotificari;
import Observer.Persoana;

public class Pacient implements Persoana {
   public String nume;
   public String prenume;
   public String numarTelefon;
   public int anNasterii;
   public String comentariu;
   public String tipulConsulatiei;
   public ClinicaNotificari clinicaNotificari;

   public Pacient(ClinicaNotificari clinicaNotificari)
   {
     this.clinicaNotificari  = clinicaNotificari;
   }
   @Override
   public void update() {
      if (clinicaNotificari.statut == "inchis") {
         System.out.println("Pacienetul " + nume + " " + prenume + " a fost informat despre inchiderea clinicei");
      }
      else if(clinicaNotificari.statut == "deschis")
      {
         System.out.println("Pacienetul " + nume + " " + prenume + " a fost informat despre deschiderea clinicei");
      }

   }
}
