import Modele.Pacient;
import Servicii.DateValidator;
import Servicii.ServiciuDatabase;
import Servicii.ServiciuMedic;

public class InregistrareFacade {
    private DateValidator dateValidator;
    private ServiciuMedic serviciuMedic;
    private ServiciuDatabase serviciuDatabase;
    public InregistrareFacade()
    {
        dateValidator = new DateValidator();
        serviciuMedic = new ServiciuMedic();
        serviciuDatabase = new ServiciuDatabase();
    }
    public String Inregistrare(Pacient pacient)
    {
        var valid = dateValidator.Validare(pacient);
        if (valid) {
            String dataProgramare = serviciuMedic.SelectareMedicDisponibil();
            serviciuDatabase.Salvare();
            return dataProgramare;
        }
        return null;
    }
}
